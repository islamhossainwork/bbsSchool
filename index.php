<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BBSSchool&College</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<!--mystyle add here-->
    <link href="css/style.css" rel="stylesheet" type="text/css">

  </head>
  <body>
    <!--header start-->
    <div class="container-fluid">
		<div class="container">
			<div class="header_blink">
				<header>
					<h1>BBS School & College</h1>
				</header>
			</div>
			
		</div>
	</div>
	<!--heder end-->
	<!--menu start-->
	<div class="container-fluid">
		<div class="container">
			<div class="header_blink">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<nav id="primary_nav_wrap">
						<?php
							include("html/menu_index.php");
						?>
						</nav>
				</div>
			</div>
			
		</div>
	</div>
	<!--menu end-->
	<!--body content start-->
		<div class="container">
			<div class="content">
				<section>
					<div class="col-md-12">
						<center><br/><p id="image"><img src="image/imageTwo.jpg" alt="BBS School & College"</div></center>
						
						
							<div class="col-md-6">
							<p id="home_page_content">
                                                        <center>
                                                            <a href="#"><h3>Class Routine For School</h3></a>
                                                            <a href="#"><h3>Class Routine For College</h3></a>
                                                        </center>
							</p>
							</div>
							<div class="col-md-6">
							<p id="home_page_content">
							<h2>History</h2>
							is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been 
							the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
							of type and scrambled it to make a type specimen book. It has survived not only five centuries,
							but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised 
							in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently
							with desktop publishing software like Aldus PageMaker including 
							<a href="">see more</a>
							</p>
							</div>						
						</p>
					</div>
					
				</section>
			
			</div>
		</div>
	<!--body content end-->
	<!--footer start-->
    <div class="container-fluid">
		<div class="container">
			<div class="footer_blink">
				<footer>
					<section>
						<p id="footer_left">
							<center>
							<strong id="right_footer">Copyright &copy 2016-  BBS School & College</strong><br/>
							<strong id="right_footer">Developed by-<a href="#">DwinIT</a></strong>
							</center>
							
						</p>
					</section>
				</header>
			</div>
			
		</div>
	</div
	<!--footer end-->
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>