<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BBSSchool&College</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	<!--mystyle add here-->
    <link href="../css/style.css" rel="stylesheet" type="text/css">

  </head>
  <body>
    <!--header start-->
    <div class="container-fluid">
		<div class="container">
			<div class="header_blink">
				<header>
					<h1>BBS School & College</h1>
				</header>
			</div>
			
		</div>
	</div>
	<!--heder end-->
	<!--menu start-->
	<div class="container-fluid">
		<div class="container">
			<div class="header_blink">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<nav id="primary_nav_wrap">
						<?php
							include("menu.php");
						?>
					</nav>
				</div>
			</div>
			
		</div>
	</div>
	<!--menu end-->
	<!--body content start-->
		<div class="container">
			<div class="content">
				<section>
					<div class="col-md-12">
						    <center><p id="image"></center>
												
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<a id="sub_link" href="#">
									<p id="home_page_content">
										<div class="link_subject">School Books</div>
									</p>
								</a>
								<a id="sub_link" href="#">
									<p id="home_page_content">
										<div class="link_subject">College Books</div>
									</p>
								</a>																	
                                                                <a id="sub_link" href="#">
									<p id="home_page_content">
										<div class="link_subject">Student Blog</div>
									</p>
								</a>
							</div>												
						</p>
					</div>
					
				</section>
			
			</div>
		</div>
	<!--body content end-->
	<!--footer start-->
    <div class="container-fluid">
		<div class="container">
			<div class="footer_blink">
				<footer>
					<section>
						<p id="footer_left">
							<center>
							<strong id="right_footer">Copyright &copy 2016-  BBS School & College</strong><br/>
							<strong id="right_footer">Developed by-<a href="#">DwinIT</a></strong>
							</center>
							
						</p>
					</section>
				</header>
			</div>
			
		</div>
	</div
	<!--footer end-->
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
  </body>
</html>