<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BBSSchool&College</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--mystyle add here-->
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <!--mystyle admin here-->
    <link href="../css/style_admin.css" rel="stylesheet" type="text/css">

    <!--javascript form validation start-->
    <script type="text/javascript">
        function validateAdmin(){
            var name = document.forms["admin_login"]["username"].value;
            var pass = document.forms["admin_login"]["password"].value;
            
            if(name == null || name == ""){
                alert("User Name Missing!!");
                return false;
            }else if(pass == null || pass ==""){
                alert("Password Missing!!");
                return false;
            }
        }
    </script>
    <!--javascript form validation end-->
  </head>
  <body>
    <!--header start-->
    <div class="container-fluid">
		<div class="container">
			<div class="header_blink">
				<header>
					<h1>BBS School & College</h1>
				</header>
			</div>			
		</div>
	</div>
	<!--heder end-->
	<!--menu start-->
	<div class="container-fluid">
		<div class="container">
			<div class="header_blink">
				<div class="col-md-4"></div>
				<div class="col-md-8">
					<nav id="primary_nav_wrap">
					<?php
					   // include("menu.php");
					?>
					</nav>
				</div>
			</div>
			
		</div>
	</div>
	<!--menu end-->
	<!--body content start-->
		<div class="container">
			<div class="content">
				<section>
                                    <br/>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <center>
                                            <form name="admin_login" onsubmit="return validateAdmin();" method="post" action="../php/admin_login.php">
                                            <p id="form_data">Username : <input type="text" name="username" placeholder="Enter User Name" /></p>
                                            <p id="form_data">Password : <input type="text" name="password" placeholder="Enter Password"/></p>
                                            <p id="form_data"><input type="submit" value="Login"/></p>                                    
                                        </form>  
                                        </center>
                                    </div>
					
				</section>
			
			</div>
		</div>
	<!--body content end-->
	<!--footer start-->
    <div class="container-fluid">
		<div class="container">
			<div class="footer_blink">
				<footer>
					<section>
						<p id="footer_left">
							<center>
							<strong id="right_footer">Copyright &copy 2016-  BBS School & College</strong><br/>
							<strong id="right_footer">Developed by-<a href="#">DwinIT</a></strong>
							</center>
							
						</p>
					</section>
				</header>
			</div>
			
		</div>
    </div>
	<!--footer end-->
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
  </body>
</html>